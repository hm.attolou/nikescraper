# -*- coding: utf-8 -*-
import argparse
import time
from datetime import date

import pandas as pd
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager


def scraper(url, output_file):
    wd = init_wd()
    wd.get(url)
    time.sleep(5)
    elem = wd.find_element_by_tag_name("body")
    no_of_pagedowns = 200
    try:
        wd.execute_script("arguments[0].click();", wd.find_element_by_xpath(".//button[@data-var='acceptBtn2']"))
    except NoSuchElementException:
        pass

    wd.save_screenshot("test.png")

    while no_of_pagedowns:
        elem.send_keys(Keys.END)
        time.sleep(0.2)
        no_of_pagedowns -= 1
    content = wd.find_elements_by_class_name("product-card__body")
    parameters = ['product-card__title', 'product-card__subtitle', ]

    df_f = pd.DataFrame(columns=['product-card__title',
                                 'product-card__subtitle',
                                 'product-price',
                                 'product-price-reduced',
                                 'cover_image',
                                 'product_url',
                                 'productID',
                                 'currency',
                                 'retrieve_date',
                                 'colors',
                                 'img_all',
                                 'description',
                                 ], dtype="object")

    for par in parameters:
        list = []
        for el in content:
            j = el.find_element_by_class_name(par)
            list.append(j.text)
        df_f.loc[:, par] = list

    prices = []
    sales = []
    imgs = []
    links = []

    print("number of products found : %d" % len(content))
    print("scraping the products information ...")
    for el in content:
        j = el.find_element_by_xpath(".//div[@data-test = 'product-price']")
        prices.append(j.text)
        try:
            j = el.find_element_by_xpath(".//div[@data-test ='product-price-reduced']")
            sales.append(j.text)
        except  NoSuchElementException:
            sales.append(None)
        j = el.find_element_by_xpath(".//img").get_attribute('src')
        imgs.append(j)
        j = el.find_element_by_xpath(".//a[@class ='product-card__link-overlay']").get_attribute('href')
        links.append(j)

    df_f.loc[:, "product_url"] = links
    df_f.loc[:, "cover_image"] = imgs
    try:
        df_f.loc[:, "product-price-reduced"] = pd.Series(sales).str.split(expand=True)[0].replace(',', '.',
                                                                                                  regex=True).astype(
            float)
        df_f.loc[:, "product-price"] = pd.Series(prices).str.split(expand=True)[0].replace(',', '.', regex=True).astype(
            float)
        df_f.loc[:, "currency"] = pd.Series(prices).str.split(expand=True)[1]
    except ValueError:
        df_f.loc[:, "product-price-reduced"] = pd.Series(sales).str.replace('$', '')
        df_f.loc[:, "product-price"] = pd.Series(prices).str.replace('$', '')
        df_f.loc[:, "currency"] = "$"

    df_f.loc[:, "productID"] = df_f.product_url.str.split("/").str[-1]

    df_f.loc[:, "retrieve_date"] = date.today()

    for index, product in df_f.iterrows():
        wd.get(product.product_url)
        time.sleep(0)
        try:
            df_f.at[index, "colors"] = \
                wd.find_element_by_class_name("description-preview__color-description").text.split(": ")[-1].split("/")

            df_f.loc[index, "productID"] = \
                wd.find_element_by_class_name("description-preview__style-color").text.split(": ")[-1]
            images_array = wd.find_elements_by_xpath(".//button[@data-sub-type='image']//img")
            images_links = []
            for i in images_array:
                images_links.append(i.get_attribute("src"))
            df_f.at[index, "img_all"] = images_links

            wd.execute_script("arguments[0].click();", wd.find_element_by_class_name("readMoreBtn"))
            df_f.loc[index, "description"] = wd.find_element_by_xpath(
                ".//div[@data-test='inline-product-description']").text
        except  NoSuchElementException:
            print("error on product %s" % product.product_url)
        except StaleElementReferenceException:
            print("ref error on product %s" % product.product_url)

    # df_f.to_csv(output_file)
    df_f.to_pickle(output_file)


def init_wd():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument('--incognito')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--verbose')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--disable-setuid-sandbox")
    wd = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    wd.set_window_size(7680, 4320)
    return wd


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('url', help='Nike.com product list page to scrape', )
    parser.add_argument('--output', '-o', default='./output.csv', help='output file destination')
    args = parser.parse_args()

    if args.url:
        scraper(url=args.url, output_file=args.output)
    else:
        raise ValueError('Must provide url from nike.com')


if __name__ == '__main__':
    main()
