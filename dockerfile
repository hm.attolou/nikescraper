FROM joyzoursky/python-chromedriver:latest
WORKDIR /usr/src/app
USER root
RUN apt-get update && apt-get install python3-distutils -y
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py
RUN python3 -m pip install selenium

ADD . /usr/src/app/
RUN pip install -r requirements.txt