# -*- coding: utf-8 -*-
import argparse
import urllib
from urllib.request import urlopen

import pandas as pd


def dw(df, folder):
    print(len(df), )
    i = 0
    df_groupby = df.groupby("product_card__title")
    for name, group in df_groupby:
        if name != "":
            # print("%d/%d group:%s, with %d products " % (i, len(df_groupby), name, len(group)))
            for index, row in group.iterrows():
                try:
                    urllib.request.urlretrieve(row["cover_image"], ("%s/%s.jpg" % (folder, row["productID"])))
                except Exception as e:
                    print(">>>>>>>>>>>>>>" + str(e) + ">>" + row["cover_image"], )
        i += 1


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('file', help='NikeScraper product DataFrame to label', type=str)
    parser.add_argument('--output', '-o', default='./imgs', type=str, help='output folder destination')
    args = parser.parse_args()

    if args.file:
        dw(pd.read_pickle(args.file).drop_duplicates("productID"), folder=args.output)
    else:
        raise ValueError('Must provide DataFrame from NikeScraper')


if __name__ == '__main__':
    main()