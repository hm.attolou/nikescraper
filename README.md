# Nike Scraper - Create Nike products dataset from nike.com pages 

PLEASE SCRAPE RESPONSIBLY
## PREREQUISITES
- Python 3
- Selenium see [here]( https://www.selenium.dev/documentation/en/selenium_installation/installing_selenium_libraries/#python )
    - `pip3 install selenium`
- Webdriver / Google Chrome see [here](https://chromedriver.chromium.org/getting-started)
- Pandas see [here](https://pandas.pydata.org/):
    - `pip3 install pandas`

## USE
Go on [nike.com](https://www.nike.com/) website and select a product page ( you can also add some filters ) and pass the URL as a paramameter for the script:

    $ python3 nikescraper.py "product list page" -o "outputfile"
    
For example, to get all items from the mens shoes and sneakers page :

    $ python3 nikescraper.py https://www.nike.com/w/mens-shoes-nik1zy7ok -o shoes.csv
    
Then if you want to download all images you can use img_dw.py:

    $ python3 img_dw.py shoes.csv -o shoes

 