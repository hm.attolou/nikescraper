# -*- coding: utf-8 -*-
import argparse
import json
import time
from datetime import date

import pandas as pd
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from sqlalchemy import create_engine
from webdriver_manager.chrome import ChromeDriverManager


def scraper(url, output_file, debug=False):
    wd = init_wd()
    wd.get(url)
    mainPage = wd.current_window_handle

    if debug:
        print("getting %s" % wd.current_url)
    time.sleep(60)
    max_pages = 200
    no_of_pagedowns = max_pages
    position_on_page = 0
    parameters = ['product-card__title', 'product-card__subtitle', ]
    try:
        wd.execute_script("arguments[0].click();", wd.find_element_by_xpath(".//button[@data-var='acceptBtn2']"))
    except NoSuchElementException:
        pass
    if debug:
        try:
            wd.save_screenshot("test.png")
        except Exception:
            print("No screenshot available")

    while no_of_pagedowns:
        try:
            wd.execute_script("arguments[0].click();", wd.find_element_by_xpath(".//button[@data-var='closeBtn']"))
        except NoSuchElementException:
            pass
        if no_of_pagedowns % 10 == 0:
            df_f = pd.DataFrame(columns=['product-card__title',
                                         'product-card__subtitle',
                                         'product-price',
                                         'product-price-reduced',
                                         'cover_image',
                                         'product_url',
                                         'productID',
                                         'currency',
                                         'retrieve_date',
                                         'colors',
                                         'img_all',
                                         'description',
                                         'sizes'
                                         'so_sizes'
                                         ], dtype="object")
            page_info = wd.find_elements_by_class_name("product-card__body")
            content = page_info[position_on_page:]
            position_on_page = len(page_info)
            if len(content) > 0:
                fetch_info(content, df_f, parameters, debug)
                save_to_file(df_f, output_file)
                fetch_details(df_f, wd, debug, mainPage)
                save_to_file(df_f, output_file)
                upload(df_f)
            if debug:
                try:
                    wd.save_screenshot("test.png")
                except Exception:
                    print("No screenshot available")

        # wd.find_element_by_tag_name("body").send_keys(Keys.END)
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        time.sleep(0.2)
        no_of_pagedowns -= 1
        if debug:
            print("page %d" % (max_pages - no_of_pagedowns))


def fetch_details(df_f, wd, debug, mainPage):
    # wd.find_element_by_tag_name("body").send_keys(Keys.CONTROL + 't')
    for index, product in df_f.iterrows():
        wd.execute_script("window.open('%s','_blank')" % product.product_url)
        wd.switch_to.window(wd.window_handles[1])
        if debug:
            print("index %d/%d" % (index, len(df_f)))
        time.sleep(1)
        try:

            df_f.loc[index, "productID"] = \
                wd.find_element_by_class_name("description-preview__style-color").text.split(": ")[-1]
            images_array = wd.find_elements_by_xpath(".//button[@data-sub-type='image']//img")
            images_links = list(map(lambda i: i.get_attribute("src"), images_array))
            df_f.at[index, "img_all"] = images_links
            df_f.at[index, "sizes"] = [x.text for x in
                                       wd.find_elements_by_xpath(".//input[@name='skuAndSize'][not(@disabled)]/..")]
            df_f.at[index, "so_sizes"] = [x.text for x in
                                          wd.find_elements_by_xpath(".//input[@name='skuAndSize'][(@disabled)]/..")]

            df_f.at[index, "colors"] = \
                wd.find_element_by_class_name("description-preview__color-description").text.split(": ")[-1].split("/")

            wd.execute_script("arguments[0].click();", wd.find_element_by_class_name("readMoreBtn"))
            df_f.loc[index, "description"] = wd.find_element_by_xpath(
                ".//div[@data-test='inline-product-description']").text

        except  NoSuchElementException as e:
            print(e)
            print("error on product %s" % product.product_url)
        except StaleElementReferenceException as e:
            print("ref error on product %s" % product.product_url)
        wd.execute_script("window.close()")
        wd.switch_to.window(mainPage)


def fetch_info(content, df_f, parameters, debug):
    for par in parameters:
        list = []
        for el in content:
            j = el.find_element_by_class_name(par)
            list.append(j.text)
        df_f.loc[:, par] = list
    prices = []
    sales = []
    imgs = []
    links = []
    if debug:
        print("number of products found : %d" % len(content))
        print("scraping the products information ...")
    for el in content:
        print("links %d/%d" % (len(links), len(df_f)))
        try:
            j = el.find_element_by_xpath(".//div[@data-test = 'product-price']")
            prices.append(j.text)
        except  NoSuchElementException:
            prices.append(None)
        try:
            j = el.find_element_by_xpath(".//div[@data-test ='product-price-reduced']")
            sales.append(j.text)
        except  NoSuchElementException:
            sales.append(None)
        try:
            j = el.find_element_by_xpath(".//img").get_attribute('src')
            imgs.append(j)
        except  NoSuchElementException:
            imgs.append(None)
        try:
            j = el.find_element_by_xpath(".//a[@class ='product-card__link-overlay']").get_attribute('href')
            links.append(j)
        except  NoSuchElementException:
            links.append(None)
    df_f.loc[:, "product_url"] = links
    df_f.loc[:, "cover_image"] = imgs
    try:
        df_f.loc[:, "product-price-reduced"] = pd.Series(sales).str.split(expand=True)[0].replace(',', '.',
                                                                                                  regex=True).astype(
            float)
        df_f.loc[:, "product-price"] = pd.Series(prices).str.split(expand=True)[0].replace(',', '.', regex=True).astype(
            float)
        df_f.loc[:, "currency"] = pd.Series(prices).str.split(expand=True)[1]
    except ValueError:
        df_f.loc[:, "product-price-reduced"] = pd.Series(sales).str.replace('$', '')
        df_f.loc[:, "product-price"] = pd.Series(prices).str.replace('$', '')
        df_f.loc[:, "currency"] = "$"
    df_f.loc[:, "productID"] = df_f.product_url.str.split("/").str[-1]
    df_f.loc[:, "retrieve_date"] = date.today()


def save_to_file(df_f, output_file):
    df_f.to_csv(output_file + ".csv")
    df_f.to_pickle(output_file + ".pkl")


def init_wd():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--start-maximized")
    chrome_options.add_argument('--incognito')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--verbose')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--disable-setuid-sandbox")
    wd = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    wd.set_window_size(7680, 4320)
    wd.set_page_load_timeout(-1)
    return wd


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('url', help='Nike.com product list page to scrape', )
    parser.add_argument('--output', '-o', default='./output.csv', help='output file destination')
    parser.add_argument('--debug', '-d', action=argparse.BooleanOptionalAction, default=False,
                        help='output debug verbose')
    args = parser.parse_args()

    if args.url:
        scraper(url=args.url, output_file=args.output, debug=args.debug)
    else:
        raise ValueError('Must provide url from nike.com')


def upload(df):
    with open("config.json") as json_file:
        config = json.load(json_file)

    engine = create_engine(
        'postgresql://' + config["user"] + ':' + config["pwd"] + '@' + config["host"] + '/' + config["db"] + '',
        echo=False)
    df.rename(columns={"product-card__title": "product_card__title", }, inplace=True)
    df.rename(columns={"product-card__subtitle": "product_card__subtitle", }, inplace=True)
    df.rename(columns={"date": "retrieve_date", }, inplace=True)
    df.rename(columns={"product-price": "product_price", }, inplace=True)
    df.rename(columns={"product-price-reduced": "product_price_reduced", }, inplace=True)
    df.retrieve_date = pd.to_datetime(df.retrieve_date, format="%Y-%m-%d")
    df.drop_duplicates(subset=["productID", "retrieve_date"], keep="last", inplace=True)
    for i in range(len(df)):
        print("upload (%d/%d)" % (i, len(df)))
        try:
            df.iloc[i:i + 1].to_sql('nikes', con=engine, if_exists="append", index=False, schema="db")
        except Exception as e:
            print(e)
            print(df.iloc[i:i + 1].product_url)


if __name__ == '__main__':
    main()